﻿namespace GoogleARCore.Examples.AugmentedImage
{
    using System.Collections;
    using System.Collections.Generic;
    using TMPro;

    using UnityEngine;

    public class TextManager : MonoBehaviour
    {
        public string ID;
        // Start is called before the first frame update
        void Start()
        {
            GetComponent<TextMeshPro>().text = "Mannequin ID:" + ID + "\nSize: 180cm\nWeight: 75kg\nStatus: Ready to purchase";
        }
    }
}
