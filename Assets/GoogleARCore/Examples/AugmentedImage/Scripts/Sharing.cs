﻿namespace GoogleARCore.Examples.AugmentedImage
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    using VoxelBusters;
    using VoxelBusters.NativePlugins;

    public class Sharing : MonoBehaviour
    {
        public bool isSharing;

        public void ShareOnSocialMedia() {
            isSharing = true;
        }

        private void LateUpdate() {
            if (isSharing == true) {
                isSharing = false;
                StartCoroutine(TakeScreenshot());
            }
        }

        IEnumerator TakeScreenshot() {
            yield return new WaitForEndOfFrame();
            Texture2D texture = ScreenCapture.CaptureScreenshotAsTexture();
            ShareSheet(texture);
            Object.Destroy(texture);
        }
        private void ShareSheet(Texture2D texture) {
            ShareSheet _shareSheet = new ShareSheet();
            _shareSheet.AttachImage (texture);

            NPBinding.Sharing.ShowView(_shareSheet, Finish);
        }

        private void Finish(eShareResult eResult) {
            Debug.Log(eResult);
        }
    }
}
